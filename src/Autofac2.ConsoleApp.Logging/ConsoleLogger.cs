using System;

namespace Autofac2.ConsoleApp.Logging
{
    public class ConsoleLogger : ILogger
    {
        public ConsoleLogger()
        {
            Log("Console Logger created.");
        }

        public void Log(string message)
        {
            Console.WriteLine($"{DateTime.Now}: {message}");
        }

        public void Info(string message)
        {
            LogWithColor(message, ConsoleColor.Green);
        }

        public void Warn(string message)
        {
            LogWithColor(message, ConsoleColor.Red);
        }

        private void LogWithColor(string message, ConsoleColor color)
        {
            var defaultColor = Console.ForegroundColor;

            Console.ForegroundColor = color;
            Log(message);
            Console.ForegroundColor = defaultColor;
        }
    }
}