namespace Autofac2.ConsoleApp.Logging
{
    public interface ILogger
    {
        void Log(string message);
        void Info(string message);
        void Warn(string message);
    }
}