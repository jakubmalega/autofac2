﻿using Autofac;

namespace Autofac2.ConsoleApp.Logging
{
    public class ModuleDefinition : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<ConsoleLogger>().As<ILogger>().Keyed<ILogger>(LoggerType.Console).SingleInstance();
            builder.RegisterType<FileLogger>().As<ILogger>().Keyed<ILogger>(LoggerType.File).PreserveExistingDefaults().SingleInstance();
        }
    }
}
