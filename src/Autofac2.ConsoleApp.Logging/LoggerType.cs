﻿namespace Autofac2.ConsoleApp.Logging
{
    public enum LoggerType
    {
        Console, File
    }
}
