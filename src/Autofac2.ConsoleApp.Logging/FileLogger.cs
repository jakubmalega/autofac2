﻿using System;
using System.IO;

namespace Autofac2.ConsoleApp.Logging
{
    public class FileLogger : ILogger
    {
        private const string LogPath = "log.txt";

        public void Log(string message)
        {
            Log("LOG", message);
        }

        public void Info(string message)
        {
            Log("INFO", message);
        }

        public void Warn(string message)
        {
            Log("WARN", message);
        }

        private void Log(string type, string message)
        {
            File.AppendAllText(LogPath, $"{DateTime.Now} {type} {message}{Environment.NewLine}");
        }
    }
}
