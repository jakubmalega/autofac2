﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Autofac2.ConsoleApp.Logging;
using Castle.DynamicProxy;

namespace Autofac2.ConsoleApp
{
    public class TvCallInterceptor : IInterceptor
    {
        private readonly ILogger _logger;

        public TvCallInterceptor(ILogger logger)
        {
            _logger = logger;
        }

        public void Intercept(IInvocation invocation)
        {
            if (invocation.Method.MemberType == MemberTypes.Method)
            {
                _logger.Warn($"{invocation.Method.Name} executing...");
                invocation.Proceed();
                _logger.Warn($"{invocation.Method.Name} executed...");
            }
            else
            {
                invocation.Proceed();
            }
            
        }
    }
}
