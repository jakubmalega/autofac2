using System;
using Autofac2.ConsoleApp.Logging;

namespace Autofac2.ConsoleApp
{
    public class InputWrapper : IInputWrapper
    {
        public InputWrapper(ILogger logger)
        {
            logger.Log("Input Wraper created.");
        }

        public string ReadCommand()
        {
            Console.Write("Enter Command: ");
            return Console.ReadLine()?.ToLower();
        }
    }
}