﻿using System;
using Autofac;
using Autofac.Extras.DynamicProxy;
using Autofac.Features.AttributeFilters;
using Autofac2.ConsoleApp.Commands;
using Autofac2.ConsoleApp.Logging;
using Autofac2.ConsoleApp.States;

using LoggingModule = Autofac2.ConsoleApp.Logging.ModuleDefinition;

namespace Autofac2.ConsoleApp
{
    static class Bootstrapper
    {
        public static IApp Configure()
        {
            var builder = new ContainerBuilder();

            builder.RegisterModule<LoggingModule>();

            builder.RegisterType<App>().As<IApp>().PropertiesAutowired();
            //// builder.RegisterType<App>().As<IApp>().PropertiesAutowired((pi, obj) => pi.Name == "CommandParser");
            
            builder.RegisterType<CommandParser>().As<ICommandParser>().WithAttributeFiltering();
            builder.RegisterType<InputWrapper>().As<IInputWrapper>();

            builder.RegisterType<TurnOnCommand>().Named<ICommand>("command").WithMetadata("CommandType", CommandType.TurnOn);
            builder.RegisterType<TurnOffCommand>().Named<ICommand>("command").WithMetadata("CommandType", CommandType.TurnOff);
            builder.RegisterType<ExitCommand>().Named<ICommand>("command").WithMetadata("CommandType", CommandType.Exit);
            builder.RegisterType<EmptyCommand>().Named<ICommand>("command").WithMetadata("CommandType", CommandType.Empty);

            builder.RegisterDecorator<ICommand>((c, inner) => new CommandDecorator(inner, c.Resolve<ILogger>()), fromKey: "command");

            builder.RegisterType<TvOffState>().Keyed<ITvState>(TvState.Off);
            builder.RegisterType<TvOnState>().Keyed<ITvState>(TvState.On);

            builder.RegisterType<Tv>().As<ITv>().EnableInterfaceInterceptors().InterceptedBy(typeof(TvCallInterceptor));

            builder.Register(c => new TvCallInterceptor(c.Resolve<ILogger>()));
                //.OnActivating(e => Console.WriteLine("Tv - On Activating"))
                //.OnActivated(e => Console.WriteLine("Tv - On Activated"))
                //.OnRelease(e =>
                //    {
                //        Console.WriteLine("Tv - Release");
                //        e.Dispose();
                //    });

            var container = builder.Build();

            return container.Resolve<IApp>();
        }
    }
}
