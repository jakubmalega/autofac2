using Autofac2.ConsoleApp.Commands;

namespace Autofac2.ConsoleApp
{
    public interface ICommandParser
    {
        ICommand Parse();
    }
}