using Autofac.Features.Indexed;
using Autofac.Features.OwnedInstances;
using Autofac2.ConsoleApp.Logging;
using Autofac2.ConsoleApp.States;

namespace Autofac2.ConsoleApp
{
    public delegate Owned<ITv> TvFactory(string name, string type);

    public class Tv : ITv
    {
        private readonly ILogger _logger;
        private readonly IIndex<TvState, ITvState> _states;

        public string Name { get; }

        public string Type { get; }

        public ITvState State { get; private set; }

        public string Info => State.Info;

        public Tv(string name, string type, ILogger logger, IIndex<TvState, ITvState> states)
        {
            Name = name;
            Type = type;

            _logger = logger;
            _states = states;

            State = states[TvState.Off];

            _logger.Log($"Tv {Name} {Type} created.");
        }

        public void TurnOn()
        {
            if (State.State == TvState.On)
            {
                _logger.Warn("Tv is already ON!");
                return;
            }

            State = _states[TvState.On];
        }

        public void TurnOff()
        {
            if (State.State == TvState.Off)
            {
                _logger.Warn("Tv is already OFF!");
                return;
            }

            State = _states[TvState.Off];
        }

        public void Dispose()
        {
            _logger.Warn("Disposing TV");
        }
    }
}