﻿using Autofac2.ConsoleApp.Commands;
using Autofac2.ConsoleApp.Logging;

namespace Autofac2.ConsoleApp
{
    public class CommandDecorator : ICommand
    {
        private readonly ICommand _inner;
        private readonly ILogger _logger;

        public CommandDecorator(ICommand inner, ILogger logger)
        {
            _inner = inner;
            _logger = logger;
        }
        public bool Execute(ITv tv)
        {
            _logger.Log($"Executing {_inner.GetType().Name}...");
            return _inner.Execute(tv);
        }
    }
}
