using System;
using System.Collections.Generic;
using System.Linq;
using Autofac2.ConsoleApp.Commands;
using Autofac2.ConsoleApp.Logging;

namespace Autofac2.ConsoleApp
{
    public class App : IApp
    {
        private readonly IList<ILogger> _loggers;
        private readonly TvFactory _tvFactory;

        public ICommandParser CommandParser { get; set; }
       
        public App(IList<ILogger> loggers, TvFactory tvFactory)
        {
            _loggers = loggers;
            _tvFactory = tvFactory;
            Log(l => l.Log("App created."));
        }

        public void Run()
        {
            Log(l => l.Log("Application is running..."));

            using (var tvOwned = _tvFactory("Panasonic", "Viera"))
            {
                var tv = tvOwned.Value;
                ICommand command;
                do
                {
                    Log(l => l.Info($"{tv.Info}"));
                    command = CommandParser.Parse();
                } while (command.Execute(tv));
            }
        }

        private void Log(Action<ILogger> action)
        {
            _loggers.ToList().ForEach(action);
        }
    }
}