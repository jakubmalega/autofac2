using Autofac2.ConsoleApp.Logging;

namespace Autofac2.ConsoleApp.Commands
{
    public class TurnOnCommand : ICommand
    {
        private readonly ILogger _logger;

        public TurnOnCommand(ILogger logger)
        {
            _logger = logger;
            _logger.Log("Turn On Command created.");
        }

        public bool Execute(ITv tv)
        {
            tv.TurnOn();
            return true;
        }
    }
}