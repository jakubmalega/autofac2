namespace Autofac2.ConsoleApp.Commands
{
    public interface ICommand
    {
        bool Execute(ITv tv);
    }
}