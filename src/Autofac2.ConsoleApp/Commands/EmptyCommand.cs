using Autofac2.ConsoleApp.Logging;

namespace Autofac2.ConsoleApp.Commands
{
    public class EmptyCommand : ICommand
    {
        private readonly ILogger _logger;

        public EmptyCommand(ILogger logger)
        {
            _logger = logger;
            _logger.Log("Empty Command created.");
        }

        public bool Execute(ITv tv)
        {
            _logger.Warn("Unknown command!");

            return true;
        }
    }
}