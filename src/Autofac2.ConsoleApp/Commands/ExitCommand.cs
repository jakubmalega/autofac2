using Autofac2.ConsoleApp.Logging;

namespace Autofac2.ConsoleApp.Commands
{
    public class ExitCommand : ICommand
    {
        private readonly ILogger _logger;

        public ExitCommand(ILogger logger)
        {
            _logger = logger;
            _logger.Log("Exit Command created.");
        }

        public bool Execute(ITv tv)
        {
            _logger.Warn("Exiting app...");

            return false;
        }
    }
}