namespace Autofac2.ConsoleApp
{
    public interface IInputWrapper
    {
        string ReadCommand();
    }
}