using System;
using Autofac2.ConsoleApp.States;

namespace Autofac2.ConsoleApp
{
    public interface ITv : IDisposable
    {
        string Name { get; }
        string Type { get; }
        ITvState State { get; }
        string Info { get; }
        void TurnOn();
        void TurnOff();
    }
}