﻿using System;

namespace Autofac2.ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            var app = Bootstrapper.Configure();
            app.Run();

            Console.ReadKey();
        }
    }
}
