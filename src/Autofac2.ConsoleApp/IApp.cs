namespace Autofac2.ConsoleApp
{
    public interface IApp
    {
        void Run();
    }
}