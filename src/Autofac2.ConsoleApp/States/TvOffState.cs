using Autofac2.ConsoleApp.Logging;

namespace Autofac2.ConsoleApp.States
{
    public class TvOffState : ITvState
    {
        public TvOffState(ILogger logger)
        {
            logger.Log("TvOffState created.");
        }

        public TvState State => TvState.Off;

        public string Info { get; } = "Tv is OFF.";
    }
}