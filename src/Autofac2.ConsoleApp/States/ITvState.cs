namespace Autofac2.ConsoleApp.States
{
    public interface ITvState
    {
        TvState State { get; }

        string Info { get; }
    }
}