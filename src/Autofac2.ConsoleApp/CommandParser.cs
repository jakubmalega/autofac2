using System;
using System.Collections.Generic;
using System.Linq;
using Autofac.Features.AttributeFilters;
using Autofac.Features.Metadata;
using Autofac2.ConsoleApp.Commands;
using Autofac2.ConsoleApp.Logging;

namespace Autofac2.ConsoleApp
{
    public class CommandParser : ICommandParser
    {
        private readonly IInputWrapper _input;
        private readonly IList<Meta<ICommand>> _commands;

        public CommandParser([KeyFilter(LoggerType.File)] ILogger logger, IInputWrapper inputWrapper, IList<Meta<ICommand>> commands)
        {
            _input = inputWrapper;
            _commands = commands;

            logger.Log("Command Parser created.");
        }

        public ICommand Parse()
        {
            var commandName = _input.ReadCommand();

            CommandType commandType;
            switch (commandName)
            {
                case "turn-on":
                    commandType = CommandType.TurnOn;
                    break;
                case "turn-off":
                    commandType = CommandType.TurnOff;
                    break;
                case "exit":
                    commandType = CommandType.Exit;
                    break;
                default:
                    commandType = CommandType.Empty;
                    break;
            }

            return _commands.Single(m => (CommandType)m.Metadata["CommandType"] == commandType).Value;
        }
    }
}